# Make your own Dockerfile

Create a container that prints it’s environment variables

Tip:
Usefull linux command: `env`

Steps:

- First just run “env” in your local shell
- Create Dockerfile
- Build & Test

Next:
- Add environment variables to “docker run” when creating your container and see them printed

## Learning goals

- Get familiar with 
  - Create Dockerfile
- Apply Dockerfile intruction: FROM, CMD
- Add environment variable to container
- Get used to
  - build images
  - run a container locally
