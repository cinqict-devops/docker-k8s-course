# Lab - Deploy Pod

ToDo

- Create pod
  `kubectl run hello-world --image dirc/webapp`
- Get status of the pod
- Describe the pod
  `kubectl describe po hello-world`
  - Check “Events” to see what just happend

## Learning goals

- Create first pod and see what happens
- Get familiar with
  - `kubectl get/describe pod`
- Apply

# Lab - Deploy Pod YAML

ToDo

- Create pod yaml
  `kubectl run hello-world --image dirc/webapp -o yaml --dry-run=client`
- Compare to pod.yaml
- Run: `kubectl apply -f pod.yaml`
- Get status of the pod
- Describe the pod
  - Compare to pod.yaml

## Learning goals

- Get familiar with
  - pod yaml
  - `kubectl apply -f`
- Apply
  - `kubectl get/describe pod`

