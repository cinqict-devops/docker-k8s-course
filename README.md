
# Docker + Kubernetes + AKS course (3 days)

Main overview

- Day 1: Docker
- Day 2: Kubernetes core
- Day 3: Kubernetes+ and AKS

## Main Goal

The basics of Docker and Kubernetes is a steep learning path. This course is ment to guide you through this first part. The goal is to enable you to make the next steps yourself.

## Labs

You learn Docker and Kubernetes by doing. Each day consist of multiple labs where you learn handson.

## Prerequisites

Either:

- SSH client (e.g. Putty, or ssh command line tool) 

or some tools (prefered)

- `git`
- `Docker`
- `kubectl`

Having some tools locally installed has preference because:

- you can apply what you have learned in your work more easily.
- you can use your favourite editor to edit Dockerfiles and k8s yaml's.

If you can't install these tools locally you can use the provided VM's to work on, which you can reach via a ssh client.

## Agenda

### Day 1 - Docker

- What is Docker
- Why containers
- How to use
  - Build
    - Dockerfile
    - Docker CLI
    - Build images
  - Run (locally)
  - Ship
    - Registries
    - Push / pull
- What’s next?

### Day 2 - Kubernetes

- What is Kubernetes
- Why Kubernetes
- How to use
  - Kubectl
  - Pods
  - Deployment
  - Services
- Tips & Tricks

### Day 3 - Kubernetes + AKS

- More K8s
  - Namespaces
  - ConfigMaps, Volumes
- What & Why AKS
- K8s + AKS LCM
- How to use
  - Services + AKS
  - Troubleshooting
- Wrap up
  - What did we not touch?
  - CNCF ecosystem

## Infra (Labs)

VM's created in AZure DevTestLab

- Ubuntu
- Docker

ACR

AKS clusters

- 2 Standard_DS2_v2 nodes
- shared kubeconfig via email
